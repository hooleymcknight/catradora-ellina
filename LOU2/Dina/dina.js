const dina = require('./bot/dina-brain');
const msg = require('./bot/message');
const patrol = require('./bot/functions');
const { Client, MessageEmbed } = require('discord.js');
const bot = new Client({
    autoReconnect: true,
    ws: {
        intents: ['GUILD_MESSAGES', 'GUILDS', 'GUILD_MESSAGE_REACTIONS']
    },
    partials: ['REACTION', 'MESSAGE', 'CHANNEL']
});

let prefix = '!';

bot.on('ready', () => {
  let HS = bot.guilds.cache.get('721967787890966548');
  let lou = HS.channels.cache.get('724108193961345075');
  let twitch = bot.guilds.cache.get('762597160126119937');
  patrol.twitchRoles(twitch);
});

bot.on('message', message => {
  let HS = bot.guilds.cache.get('721967787890966548');
  let twitch = bot.guilds.cache.get('762597160126119937');
  msg.route(bot, message, prefix);

  if(message.content === 'sendit') {
      const embed = new MessageEmbed()
      .setTitle('React on this message to get you some new roles.')
      .setColor(0x4f1665)
      .setDescription(patrol.embed_roles);
      twitch.channels.cache.get('771597376124747789').send(embed);
  }
});

bot.login(dina.login());
