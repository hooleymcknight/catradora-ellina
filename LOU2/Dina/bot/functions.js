function makeBad(guild, baddies, bad_role) {
    baddies = removeBots(baddies);
    if(baddies.length > 0) {
        // remove bad role from everyone else
        console.log(bad_role);
        let goodies = bad_role.members.map(x => x.id);
        console.log(bad_role.members);
        //let goodies = guild.members.cache.filter(m => m.roles.cache.has('780308890600210452'));
        console.log(goodies)
        for(var grill in goodies) {
            guild.members.cache.find(x => x.id === goodies[grill]).roles.remove(bad_role);
        }
        // make baddie bad
        for(i=0; i < baddies.length; i++) {
            baddies[i].roles.add(bad_role);
        }
    }
}

function removeBots(baddies) {
    for(i=0; i < baddies.length; i++) {
        if(baddies[i].roles.cache.find(x => x.name === 'Bots')) {
            baddies.splice(baddies.indexOf(baddies[i]), 1);
        }
    }
    return baddies;
}

function spritz(msg) {
    let bad_role = msg.guild.roles.cache.find(x => x.name === 'BAD');
    let baddies = [];
//    if(msg.mentions.users.size > 0) {
        // set a specific person(s) to be bad
        //let bad_users = msg.mentions.users.map(x => x.id);
        //for(i=0; i<bad_users.length; i++) {
            //baddies.push(msg.guild.members.cache.find(x => x.id === bad_users[i]));
        //}
        //console.log('baddies', baddies);
        //makeBad(msg.guild, baddies, bad_role);
    //}
    //else {
        // get whoever sent the last message
        msg.channel.messages.fetch({ limit: 2 }).then(messages => {
            console.log(messages.last());
            console.log(messages.last().member);
            baddies.push(messages.last().member);
            console.log(baddies)
            makeBad(msg.guild, baddies, bad_role);
        }).catch(console.error);
    //}
}

function roleReactions(message) {
    message.react('731079728169091153'); // catra
    message.react('731055452174942280'); // ellie
    message.react('759715892598145094'); // sus
    message.react('759714271864160286'); // eso
    message.react('759712179367706654'); // overwatch
    message.react('759717188138696706'); // sea of thieves
    message.react('771628976732110859'); // Bly derp
    //message.react('759714412016828428'); // valorant
    //message.react('759714922724327444'); // apex
    message.react('747816645053120523'); // D-face
    message.react('759717314026668072'); // space cowboy
    message.react('759715989734162432'); // Bones
    message.react('745636074960977952'); // Aang
    message.react('759713721479725076'); // Joel
    //message.react('757844568405442620'); // Ampersand
    message.react('759714548981694504'); // gay squad
    message.react('🇹');
    message.react('🇸');
    message.react('🇭');
}

function getEmojis(guild, message, channel, member) {
    const filter = (reaction, user) => {
        return user.id === member.id;
    }
    const collector = message.createReactionCollector(filter, { time: 48000, dispose: true });

    collector.on('collect', (reaction, reactionCollector) => { // give the roles
        giveRole(guild, member, processRole(guild, reaction.emoji.name));
    });

    collector.on('remove', (reaction, reactionCollector) => { // jk delete it
        removeRole(guild, member, processRole(guild, reaction.emoji.name));
    });

    collector.on('end', (reaction, reactionCollector) => { // tell them it's over
        channel.send(`I gave you the roles you asked for. If you need help later, ` +
        `just type \`\`!roles\`\` again.`);
    })
}

function twitchRoles(guild) {
    guild.channels.cache.get('771597376124747789').messages.fetch({ limit: 5 }).then(messages => {
        var message = messages.find(x => x.id === '771810925517996062');

        const twitch_filter = (reaction, user) => {
            return user.id !== 'null';
        }
        const twitch_collector = message.createReactionCollector(twitch_filter, { dispose: true });
        console.log(twitch_collector);

        twitch_collector.on('collect', (reaction, user) => { // give the roles
            console.log('collected');
            var member = guild.members.cache.find(x => x.id === user.id);
            giveRole(guild, member, processRole(guild, reaction.emoji.name));
        });

        twitch_collector.on('remove', (reaction, user) => { // jk delete it
            var member = guild.members.cache.find(x => x.id === user.id);
            removeRole(guild, member, processRole(guild, reaction.emoji.name));
        });

        twitch_collector.on('end', collected => {
            console.log('I am stopping');
        })
    });
}

function giveRole(guild, member, role_name) {
    console.log(role_name);
    if(typeof(role_name) !== 'undefined') {
        let role = guild.roles.cache.find(x => x.name === role_name);
        member.roles.add(role);
    }
}

function removeRole(guild, member, role_name) {
    if(typeof(role_name) !== 'undefined') {
        let role = guild.roles.cache.find(x => x.name === role_name);
        member.roles.remove(role);
    }
}

function processRole(guild, emoji) {
    switch (guild.id) {
        case "762597160126119937":
            return embed_emojis[emoji];
            break;
        case "721967787890966548":
            return role_emojis[emoji];
            break;
    }
}

let role_emojis = {
    'catra': 'Cat Girls',
    'ellie': 'Gay for Ellie',
    'joel': 'Clickers',
    'eso': 'Daggerfall Covenant',
    'derp': 'Wedding Guests',
    //'apex': 'Apex',
    'overwatch': 'Overwatch',
    'sot': 'Sea of Thieves',
    'Dface': 'Serial Killers',
    'alien_cowboy': 'Filthy Xenos',
    'sus': 'Not Fall Guys',
    'bones': 'Squints',
    'aang': 'Benders',
    'gay': 'Gay Squad',
    //'ampersand': 'Keely/Catra Stans',
    '🇹': 'they/them',
    '🇸': 'she/her',
    '🇭': 'he/him'
}

let roles_text = `Let me know what roles you need by reacting with an emoji: \n` +
    `<:catra:731079728169091153> She-Ra\n<:ellie:731055452174942280> Last of Us 2\n` +
    `<:sus:759715892598145094> Among Us\n<:eso:759714271864160286> Elder Scrolls Online\n` +
    `<:overwatch:759712179367706654> Overwatch\n<:sot:759717188138696706> Sea of Thieves\n` +
    `<:derp:771628976732110859> Bly Manor\n` +
    `<:Dface:747816645053120523> Dead by Daylight\n<:alien_cowboy:759717314026668072> Stellaris\n` +
    `<:bones:759715989734162432> Bones\n<:aang:745636074960977952> Avatar: the Last Airbender + The Legend of Korra\n` +
    `<:joel:759713721479725076> Last of Us\n` +
    `And <:gay:759714548981694504> if you're in the Gay Squad, plus whatever your preferred pronouns are:\n` +
    `🇹 they/them, 🇸 she/her, 🇭 he/him`;

let embed_emojis = {
    'adora': 'She-Ra',
    'ellie': 'Last of Us',
    'sus': 'Among Us',
    'overwatch': 'Overwatch',
    'derp': 'Bly Manor',
    'ashley': 'Critical Role',
    'dbd': 'Dead by Daylight',
    'phasmophobia': 'Phasmophobia',
    'gay': 'LGBTQ+',
    '🇹': 'they/them',
    '🇸': 'she/her',
    '🇭': 'he/him'
}

let embed_roles = `<:adora:731080569722765322> She-Ra\n<:ellie:731055452174942280> Last of Us, Parts I & II\n` +
    `<:sus:759715892598145094> Among Us\n<:overwatch:759712179367706654> Overwatch\n` +
    `<:derp:771628976732110859> Bly Manor\n<:ashley:771639657502867476> Critical Role\n` +
    `<:dbd:771643407398862849> Dead by Daylight\n<:phasmophobia:771645089151582228> Phasmophobia\n` +
    `And <:gay:771641135599845387> if you identify as LGBTQ+, plus whatever your preferred pronouns are:\n` +
    `🇹 they/them, 🇸 she/her, 🇭 he/him`;

function rolesInfo() {
    return roles_text;
}

module.exports = { spritz, rolesInfo, roleReactions, getEmojis, twitchRoles, embed_roles, embed_emojis }
