const fs = require('fs');
const config = require('config.json');
const ConfigParser = require('configparser');

const conf = new ConfigParser();
conf.read('H:/Documents/Bots/LOU2/keys/config.ini');
conf.sections();

function login() {
    return conf.get('DINA', 'discord');
}

module.exports = { login };
