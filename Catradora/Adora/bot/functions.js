function introReactions(message) {
    message.react('731079728169091153');
    message.react('731055452174942280');
    message.react('759715892598145094');
    message.react('759714271864160286');
    message.react('759712179367706654');
    message.react('759717188138696706');
    message.react('759714412016828428');
    message.react('759714922724327444');
    message.react('747816645053120523');
    message.react('759717314026668072');
    message.react('759715989734162432');
    message.react('745636074960977952');
    message.react('759713721479725076');
    message.react('757844568405442620');
    message.react('759714548981694504');
    message.react('🇹');
    message.react('🇸');
    message.react('🇭');
}

function getEmojis(guild, message, channel, member) {
    const filter = (reaction, user) => {
        return user.id === member.id;
    }
    const collector = message.createReactionCollector(filter, { time: 48000, dispose: true });

    collector.on('collect', (reaction, reactionCollector) => { // give the roles
        giveRole(guild, member, processRole(reaction.emoji.name));
    });

    collector.on('remove', (reaction, reactionCollector) => { // jk delete it
        removeRole(guild, member, processRole(reaction.emoji.name));
    });

    collector.on('end', (reaction, reactionCollector) => { // tell them it's over
        channel.send(`I gave you the roles you asked for. If you need help later, ` +
        `just type \`\`!roles\`\` and <@728462311332905021> will help you.`);
    })
}

function giveRole(guild, member, role_name) {
    let role = guild.roles.cache.find(x => x.name === role_name);
    member.roles.add(role);
}

function removeRole(guild, member, role_name) {
    let role = guild.roles.cache.find(x => x.name === role_name);
    member.roles.remove(role);
}

function processRole(emoji) {
    return role_emojis[emoji];
}

let role_emojis = {
    'catra': 'Cat Girls',
    'ellie': 'Gay for Ellie',
    'joel': 'Clickers',
    'eso': 'Daggerfall Covenant',
    'valorant': 'VALORANT',
    'apex': 'Apex',
    'overwatch': 'Overwatch',
    'sot': 'Sea of Thieves',
    'Dface': 'Serial Killers',
    'alien_cowboy': 'Filthy Xenos',
    'sus': 'Not Fall Guys',
    'bones': 'Squints',
    'aang': 'Benders',
    'gay': 'Gay Squad',
    'ampersand': 'Keely/Catra Stans',
    '🇹': 'they/them',
    '🇸': 'she/her',
    '🇭': 'he/him'
}

let intro_text = `Let me know what roles you need by reacting with an emoji: \n` +
    `<:catra:731079728169091153> She-Ra\n<:ellie:731055452174942280> Last of Us 2\n` +
    `<:sus:759715892598145094> Among Us\n<:eso:759714271864160286> Elder Scrolls Online\n` +
    `<:overwatch:759712179367706654> Overwatch\n<:sot:759717188138696706> Sea of Thieves\n` +
    `<:valorant:759714412016828428> VALORANT\n<:apex:759714922724327444> Apex Legends\n` +
    `<:Dface:747816645053120523> Dead by Daylight\n<:alien_cowboy:759717314026668072> Stellaris\n` +
    `<:bones:759715989734162432> Bones\n<:aang:745636074960977952> Avatar: the Last Airbender + The Legend of Korra\n` +
    `<:joel:759713721479725076> Last of Us\n<:ampersand:757844568405442620> Aly & AJ\n` +
    `And <:gay:759714548981694504> if you're in the Gay Squad, plus whatever your preferred pronouns are:\n` +
    `🇹 they/them, 🇸 she/her, 🇭 he/him`;

function intro() {
    return intro_text;
}

module.exports = { getEmojis, intro, introReactions }
